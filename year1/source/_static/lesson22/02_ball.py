import math
import sys

import pygame
from pygame.color import THECOLORS

pygame.init()

WIDTH = 1280
HEIGHT = 800

screen = pygame.display.set_mode((WIDTH, HEIGHT))

circle_center = (WIDTH // 2, HEIGHT // 2)
circle_radius = 50
circle_color = THECOLORS['purple2']

SPEED = 20

current_angle = -10


def get_move(angle):
    angle = angle / 180. * math.pi
    return (
        int(SPEED * math.cos(angle)),
        int(SPEED * math.sin(angle)))


def move_circle(center, move):
    # TODO(1): функция возвращает новый центр окружности после смещения.
    ...


def get_angle(center):
    # TODO(2): Функция принимает текущий центр круга и определяет,
    #  под каким углом он должен продолжать движение.
    ...

move = get_move(current_angle)

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()

    screen.fill(THECOLORS['black'])

    # TODO(3):
    #  * Вычислить угол движения круга.
    #  * Вычислить вектор движения
    #  * Вычислить новый центр окружности

    pygame.draw.circle(screen, circle_color, circle_center, circle_radius)

    pygame.display.flip()
    pygame.time.wait(33)
