﻿Easygui
=======

Easygui — библиотека, предназначенная для разработки простого графического
интерфейса.

Официальная документация: https://easygui.readthedocs.io/en/master/api.html

Начало работы
-------------

В первую очередь импортируем модуль easygui в файл с кодом::

    import easygui
    
Описание функций библиотеки
----------------------------------------------

.. function :: easygui.buttonbox(msg='', title=' ', choices=('Button[1]', 'Button[2]'), 
   image=None, images=None, default_choice=None, cancel_choice=None)

   Создать окно с кнопками из кортежа ``choices``, заголовком окна ``title`` и текстом ``msg``.
   Также можно задать:
     -  изображение:  ``image`` = 'C:/Python/Images/123.png'
     -  несколько изображений: ``images`` = ['games/1.png', 'games/2.png']
     -  выделенную кнопку:  ``default_choice`` =  'Button[1]'
     -  кнопку отмены: ``cancel_choice`` = 'Button[2]'
   Окно возвращает строку, равную тексту на выбранной кнопке или имени файла с изображением.

.. function :: easygui.msgbox(msg='(Your message goes here)', title=' ', ok_button='OK',
   image=None)

   Создать текстовое сообщение с текстом msg, заголовком окна ``title`` и кнопкой ``ok_button``.
   Окно возвращает текст на ``ok_butto``n.
   
.. function :: easygui.integerbox(msg='', title=' ', default=None, lowerbound=0, 
   upperbound=99, image=None, root=None)
   
   Создать окно с полем для ввода целого числа в диапазоне от ``lowerbound`` до ``upperbound``.
   Дополнительно можно указать введённое по умолчанию значение ``default``.
   Окно возвращает целое число, введённое в текстовое поле.
   
.. function :: easygui.boolbox(msg='Shall I continue?', title=' ', choices=('[Y]es', '[N]o'),
   image=None, default_choice='Yes', cancel_choice='No')
   
   Создать окно с выбором "Да" и "Нет" (кортеж ``choices``). 
   Окно возвращает True если выбрана кнопка, отличная от ``cancel_choice`` и False, если 
   выбрана кнопка ``cancel_choice``.

Задание
-------

Используя шаблон проекта, реализуйте игры «Камень, ножницы, бумага» и
«Угадай число»::

    import easygui


    def rock_paper_scissors():
        easygui.msgbox('Здесь будет игра "Камень, ножницы, бумага"')


    def guess_the_number():
        easygui.msgbox('Здесь будет игра "Угадай число"')


    games = [
        'Камень, ножницы, бумага',
        'Угадай число'
    ]

    games_entry_points = [
        rock_paper_scissors,
        guess_the_number
    ]

    while True:
        res = easygui.buttonbox('Выбери игру!', choices=games)
        if res is None:
            break
        games_entry_points[games.index(res)]()

Шаблон проекта в архиве: `games.zip <../_static/lesson13/games.zip>`_
