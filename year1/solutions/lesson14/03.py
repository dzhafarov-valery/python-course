file_name = input('File name:')
max_lines = int(input('max lines:'))

f = open(file_name, 'r')
n = 1
file_num = 1
out = open(f'{file_num}.txt', 'w')
for line in f:
    print('line: ', n)
    out.write(line)
    if n % max_lines == 0:
        print('next file')
        file_num += 1
        out.close()
        out = open(f'{file_num}.txt', 'w')
    n += 1
out.close()
f.close()
