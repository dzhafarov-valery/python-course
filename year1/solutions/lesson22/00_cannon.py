"""
+-----------------------+
|   ||||||              | <- target
|                       |
|                       |
|           *           | <- bullet
|                       |
|                       |
|          / \          | <- cannon
+-----------------------+
"""

import random
import sys

import pygame
from pygame.color import THECOLORS

pygame.mixer.init(22050, -16, 2, 64)
pygame.init()

WIDTH = 640
HEIGHT = 480

screen = pygame.display.set_mode((WIDTH, HEIGHT))


class Target:
    def __init__(self):
        self.color = THECOLORS['red4']
        self.speed = 10
        left = 0
        top = 0
        width = 100
        height = 50
        self.rect = pygame.rect.Rect(left, top, width, height)

    def draw(self):
        pygame.draw.rect(screen, self.color, self.rect)

    def move(self):
        self.rect.move_ip(self.speed, 0)

        if self.rect.right == WIDTH:
            self.speed *= -1
        elif self.rect.left == 0:
            self.speed *= -1


class Bullet:
    def __init__(self):
        self.center = (WIDTH // 2, HEIGHT - 50)
        self.radius = 5
        self.color = THECOLORS['grey']
        self.speed = 3

    def draw(self):
        pygame.draw.circle(screen, self.color, self.center, self.radius)

    def move(self):
        self.center = (self.center[0], self.center[1] - self.speed)


class Cannon:
    def __init__(self):
        self.color = THECOLORS['green3']
        height = 50
        width = 50
        self.points = (
            (WIDTH // 2, HEIGHT - height),
            (WIDTH // 2 - width // 2, HEIGHT),
            (WIDTH // 2 + width // 2, HEIGHT)
        )

    def draw(self):
        pygame.draw.polygon(screen, self.color, self.points)


colors = list(THECOLORS.values())
def get_random_color():
    return random.choice(colors)


def handle_hit(bullet, target):
    if target.rect.collidepoint(bullet.center):
        target.color = get_random_color()


cannon = Cannon()
target = Target()
bullet = Bullet()

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()

    screen.fill(THECOLORS['black'])

    target.move()
    bullet.move()

    if bullet.center[1] <= 0:
        bullet = Bullet()

    if target.rect.collidepoint(bullet.center):
        target.color = get_random_color()
        bullet = Bullet()

    cannon.draw()
    target.draw()
    bullet.draw()

    pygame.display.flip()
    pygame.time.wait(33)
