def min_max(lst):
    if len(lst) == 0:
        return None, None

    minimum = lst[0]
    maximum = lst[0]

    for item in lst:
        if item < minimum:
            minimum = item

        if item > maximum:
            maximum = item

    return minimum, maximum


assert min_max([]) == (None, None)
assert min_max([1]) == (1, 1)
assert min_max([1, 1]) == (1, 1)
assert min_max([1, 2]) == (1, 2)
assert min_max([2, 1]) == (1, 2)
assert min_max([14, 3, 14, 92, 15]) == (3, 92)

