def zip_sum_longest(lst1, lst2):
    result = []
    last = min([len(lst1), len(lst2)])
    for i in range(0, last):
        result.append(lst1[i] + lst2[i])

    tail = lst1
    if len(lst2) > len(lst1):
        tail = lst2

    for i in range(last, len(tail)):
        result.append(tail[i])

    return result


assert zip_sum_longest([], []) == []
assert zip_sum_longest([1], []) == [1]
assert zip_sum_longest([], [1]) == [1]
assert zip_sum_longest([1], [2]) == [3]
assert zip_sum_longest([1], [2, 1]) == [3, 1]
assert zip_sum_longest([2, 1], [1]) == [3, 1]

