Методы строк
============

.. function :: s.lower()
               s.upper()
               s.title()

   Методы приводят строку к нижнему, верхнему и title регистру соответственно::

     >>> 'world'.upper()
     'WORLD'
     >>> 'HELLO'.lower()
     'hello'
     >>> 'hello'.upper()
     'HELLO'
     >>> 'hello'.title()
     'Hello'

.. function :: s.count(sub)

   Возвращает количество вхождений подстроки ``sub`` в строку ``s``.

.. function :: s.find(sub)

   Возвращает минимальный индекс вхождения подстроки ``sub`` в строку ``s``::

     >>> 'otto'.find('t')
     1

.. function :: s.rfind(sub)

   Возвращает максимальный индекс вхождения подстроки ``sub`` в строку ``s``::

     >>> 'otto'.rfind('t')
     2

.. function :: s.ljust(width)

   Возвращает строку, выровненную по левой границе поля ширины ``width``.
   Строка дополняется пробелами справа::

     >>> 'hello'.ljust(10)
     'hello     '

.. function :: s.rjust(width)

   Возвращает строку, выровненную по правой границе поля ширины ``width``.
   Строка дополняется пробелами слева::

     >>> 'hello'.rjust(10)
     '     hello'

.. function :: s.replace(old, new)

   Возвращает строку, в которой все вхождения подстроки ``old`` заменены на
   подстроку ``new``.


Задачи
------

1. Пользователь вводит фамилию, имя и отчество. Приложение должно вывести
   фамилию и инициалы. Пример::

     Фамилия: Ершов
     Имя: Андрей
     Отчество: Петрович

     Ершов А. П.

2. Доработать приложение из предыдущей задачи так, чтобы программа исправляла
   регистр символов. Пример::

     Фамилия: ерШоВ
     Имя: андрей
     Отчество: петрович

     Ершов А. П.

3. Пользователь вводит слово. Подсчитать количество символов ‘a’ в нем. Пример::

     word: abracadabra

     5

4. Пользователь вводит строку и два слова. Приложение заменяет все вхождения
   первого слова на второе. Пример::

     > To be or not to be.
     Find: be
     Replace: eat

     To eat or not to eat.

5. Приложение принимает на вход строку и заменяет все вхождения буквы «ё» на
   букву «е».

6. Пользователь вводит строку. Нужно удалить из нее первое слово. Разделителем
   слов считать пробел. Пример::

     > Hello, World!
     World

7. Пользователь вводит строку. Нужно удалить из нее последнее слово.

8. Пользователь вводит строку, содержащую два слова. Приложение выводит их в
   обратном порядке. Пример::

     Harry Potter
     Potter Harry

9. Написать приложение, выполняющее транслитерацию введенного слова. Пример::

     > Привет

     Privet
