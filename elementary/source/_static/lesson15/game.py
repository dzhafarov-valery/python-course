maze = [
    "## #########",
    "##        ##",
    "####### ####",
    "#F## #  ## #",
    "#   ## ##  #",
    "###       ##",
    "#########@##"
]

WALL = '#'
DOOR = '@'
KEY = 'F'
UP = "u", "up"
DOWN = "d", "down"
RIGHT = "r", "right"
LEFT = "l", "left"

key = None

x = 2
y = 0
px = x
py = y

def printBoard(x, y):
    for i in range(7):
        for j in range(12):
            if x == j and y == i:
                print("0", end="")
            elif maze[i][j] == KEY and key:
                print(" ", end="")
            else:
                print(maze[i][j], end="")
        print()

while True:
    printBoard(px, py)
    x, y = px, py
    direction = input("up, down, left or right: ")
    if direction in LEFT:
        x -= 1
    elif direction in RIGHT:
            x += 1
    elif direction in UP:
            y -= 1
    elif direction in DOWN:
            y += 1
    else:
        print("Invalid input")

    if maze[y][x] != WALL:
        if maze[y][x] == DOOR and not key:
            print("Key is required!")
        elif maze[y][x] == DOOR and key:
            print("You've escaped the maze!")
            px, py = x, y
            printBoard(px, py)
            break
        elif maze[y][x] == KEY:
            print("Key found!")
            key = KEY
            px, py = x, y
        else:
            px, py = x, y
    else:
        print("There's wall!")
